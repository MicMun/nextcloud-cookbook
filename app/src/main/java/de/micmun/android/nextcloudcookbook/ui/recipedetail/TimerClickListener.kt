/*
 * TimerClickListener.kt
 *
 * Copyright 2023 by MicMun
 */
package de.micmun.android.nextcloudcookbook.ui.recipedetail

import android.view.View

/**
 * Listener for timer actions.
 *
 * @author MicMun
 * @version 1.0, 19.02.23
 */
interface TimerClickListener {
   fun onTimerClicked(view: View)
}
